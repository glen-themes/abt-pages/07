// akalynn supremacy

var cjaj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='150.000000pt' height='150.000000pt' viewBox='0 0 150.000000 150.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,150.000000) scale(0.100000,-0.100000)' stroke='none'> <path d='M476 1267 c-249 -98 -363 -148 -373 -163 -11 -17 -14 -92 -14 -389 0 -363 1 -369 22 -391 20 -22 537 -234 570 -234 7 0 137 48 287 107 188 73 277 113 284 127 7 12 11 151 12 383 l1 364 105 42 c114 46 132 65 77 85 -61 22 -20 36 -600 -193 l-169 -66 -199 77 c-109 43 -198 79 -196 80 8 9 565 224 578 224 9 0 79 -25 155 -55 77 -30 149 -55 162 -55 12 0 39 7 59 15 32 13 35 17 22 30 -19 19 -372 155 -401 155 -13 -1 -184 -65 -382 -143z m-16 -336 c113 -45 214 -81 225 -81 11 0 123 41 248 90 126 50 230 90 233 90 2 0 3 -149 2 -331 l-3 -331 -200 -79 c-110 -43 -208 -82 -217 -85 -17 -5 -18 7 -18 165 0 208 4 202 -150 255 l-106 37 -47 -18 c-26 -10 -46 -22 -44 -28 2 -5 53 -28 113 -50 60 -22 115 -44 122 -48 18 -12 18 -320 0 -315 -7 2 -109 41 -225 88 l-213 85 0 332 0 332 38 -13 c20 -8 129 -50 242 -95z'/> </g> </svg>";

document.documentElement.style.setProperty('--glenSVG','url("' + cjaj + '")');

// =>
$(document).ready(function(){
    $(".tumblr_preview_marker___").remove();

    $("container").each(function(){
        $(this).wrap("<div class='spoop'>");
        $(this).parent().wrap("<div id='vertigo-b'>");
        $(this).parent().parent().wrap("<div id='vertigo-a'>");
        $(this).parent().parent().parent().wrap("<div id='horizontal-b'>");
        $(this).parent().parent().parent().parent().wrap("<div id='horizontal-a'>");
    })

    $("a[title], img[title]").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:50,
        tip_fade_speed:0,
        attribute:"title"
    });

    $("[music-name]").each(function(){
        if($(this).next().is("audio")){
            $(this).add($(this).next()).wrapAll("<div class='muplayer'>")
        } else {
            $(this).wrap("<div class='muplayer'>")
        }
    })

    $(".muplayer").each(function(){
        $(this).prepend("<i class='ph-music-notes-simple-fill notife'></i>");
        $(this).prepend("<div class='controles'></div>");
    })

    $(".controles").each(function(){
        $(this).prepend("<i class='ph-play-fill jouer'></i>");
        $(this).append("<i class='ph-pause-fill pauser'></i>");
    })

    var lait = $("audio[music]")[0];
    var lait_ele = $("audio[music]");

    function mehanix(){
        $(".controles").click(function(){
            if(lait.paused){
                // if paused, play
                $(".jouer").hide();
                $(".pauser").show();
                lait.play();
            } else {
                // if it's playing, pause
                $(".jouer").show();
                $(".pauser").hide();
                lait.pause();
            }
        });
    }

    // if: autoplay
    if(lait_ele.is("[autoplay]")){
        $(".jouer").hide();
        $(".pauser").show();

        mehanix()
    } else {
        // if NOT autoplay
        mehanix()
    }

    lait.onended = function(){
        $(".jouer").show();
        $(".pauser").hide();
    }

    $("[left-sidebar] + [main-right]").each(function(){
        $(this).add($(this).prev()).wrapAll("<div class='mc'>")
    })

    $("section[top], .mc").each(function(){
        $(this).wrap("<div table-row>")
    })

    $("[main-right]").each(function(){
        $(this).wrap("<div class='mainright'>")
    })

    $("[big-title][flashing-cursor='yes']").each(function(){
        $(this).append("<div class='flooshed'>")
    })

    $("[quote]").each(function(){
        $(this).wrap("<div class='quo-table'>");
        $(this).parent().wrap("<div class='quotecont'>")
    })

    $(".quo-table").each(function(){
        $(this).prepend("<div class='qw-left'>");
        $(this).append("<div class='qw-right'>");
    })

    $(".quotecont").each(function(){
        $(this).append("<div class='tricont'>");
    })

    $(".tricont").each(function(){
        $(this).prepend("<div class='tri-over'>");
        $(this).append("<div class='tri-under'>");
    })

    $(".quotecont + [icon-image]").each(function(){
        $(this).appendTo($(this).prev());
    })

    $(".quo-table + .tricont").each(function(){
        $(this).add($(this).prev()).wrapAll("<div class='fern'>")
    })

    $("[big-title], .quotecont").each(function(){
        $(this).wrap("<div table-row>");
        if($(this).is("[big-title]")){
            $(this).parent().addClass("tb-head")
        }
    })

    $("column[left]").each(function(){
        if($(this).next().is("column[right]")){
            $(this).add($(this).next()).wrapAll("<div class='colz'>")
        } else {
            $(this).wrap("<div class='colz'>")
        }
    })

    $("column[right]").each(function(){
        if(!$(this).prev("column[left]").length){
            if(!$(this).parent().is(".colz")){
                $(this).wrap("<div class='colz'>");
            }
        }

        $(this).wrapInner("<div table>");
    })

    $("column").each(function(){
        $(this).wrap("<div class='col-ind'>");
    })

    $(".colz").each(function(){
        $(this).wrapInner("<div class='col-cov'>");
    })

    $("[labels-box]").each(function(){
        var tnt = $(this).html();
        tnt = tnt.replaceAll("* ","<li row>");
        $(this).html(tnt)
    })

    $("li[row]").each(function(){
        if($("[labels-box]").is("[include-colon='yes']")){
            var rowlab = $(this).html().split(":")[0] + ":";
            var rowdet = $(this).html().split(":").slice(1).join(":");
        } else {
            var rowlab = $(this).html().split(":")[0];
            var rowdet = $(this).html().split(":").slice(1).join(":");
        }

        $(this).html(
            "<span row-label>" + rowlab + "</span>"
            + "<span row-detail>" + rowdet + "</span>"
        )
    })

    $("[custom-links]").each(function(){
        $(this).wrapInner("<div class='ciels'>")
    })

    $("[custom-links] a").each(function(){
        $(this).wrapInner("<span>")
    })

    $("[textbox-title]").each(function(){
        $(this).wrap("<div class='tb-title'>");
    });

    $("[textbox-text]").each(function(){
        $(this).wrap("<div table-row>");
        $(this).wrapInner("<div class='scone'>")
    })

    $("column[left]").each(function(){
        var that = this;
        setTimeout(function(){
            var hh = $(that).height();
            var htz = $(".tb-title").outerHeight();
            var flesk = hh - htz;
            $("[textbox-text]").height(flesk);
            document.documentElement.style.setProperty("--TOH",flesk + "px");
            $(".spoop").addClass("hallo");

            $("container").children().unwrap();
        },269)
    })

    $(".scone").each(function(){
        $(this).wrapInner("<div class='creme'>");

        $(this).contents().filter(function(){
            return this.nodeType === 3  && this.data.trim().length > 0
        }).wrap("<span>");

        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })

    // image art credits
    $("img[sidebar-image]").each(function(){
        if(!$(this).is("title")){
            if($(this).attr("src").indexOf("66.media.tumblr.com/878f42eb223c4a51593867a95d8d954a/tumblr_phsfneeqCF1qg2f5co3_r1_640.png") > -1){
                $(this).attr("title","artwork by ipaanbaa (deviantart)");
            }
        }
    })

    $("img[icon-image]").each(function(){
        if(!$(this).is("title")){
            if($(this).attr("src").indexOf("66.media.tumblr.com/c0dca377c4946efa9f64c1ece591b435/tumblr_phsfneeqCF1qg2f5co4_r1_500.png") > -1){
                $(this).attr("title","artwork by @guttertongue");
            }
        }
    })

    $("img[title]:not([title=''])").each(function(){
        $(this).css("cursor","crosshair")
    })

    if($.browser.mozilla){
        var sbh = $("[sidebar-image]").height();
        var st = $("[small-title]").height();
        var bt = $(".tb-head").height();
        var qt = $(".tb-head + [table-row]:has(.quotecont)").height();

        var quoi = sbh - st - bt- qt;

        $(".mainright").height(quoi);
        document.documentElement.style.setProperty("--TOH",quoi + "px");
    }

    var customize_page = window.location.href.indexOf("/customize") > -1;
    var on_main = window.location.href.indexOf("/customize") < 0;

    if(customize_page){
        $("body").append('<div class="le_notice">Please read <a href=\"https://docs.google.com/presentation/d/1Uy1oSlX43sw5Ip4pHMXM12w8ztII86enwTs1NGIwf6g/edit?usp=sharing\" target=\"_blank\">the guide</a> to help with editing :)</div>');

        $(".le_notice").click(function(){
            $(this).fadeOut(450);
        })
    }
})//end ready
